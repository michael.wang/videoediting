#! /bin/bash

if [ "$#" -eq 0 ]; then
  echo "Usage: ffmpeg_trim [IN_FILE] [START_POSITION] [DURATION] [OUT_FILE]"
  echo "Fast video trimming without leading and/or tailing blank frames."
  echo ""
  echo "Examples:"
  echo "  ffmpeg input.mp4 00:12:34 00:00:10 output.mp4 Trim 10 seconds of video starting from 12 minutes 34 seconds."
  exit 0
fi



SEC_PER_MIN=60
SEC_PER_HOR=60*60
to_s () {
  POS="${1}"
  arr=(${POS//:/ })
  hh=${arr[0]}
  mm=${arr[1]}
  ss=${arr[2]}
  echo "$hh * ${SEC_PER_HOR} + $mm * ${SEC_PER_MIN} + $ss" | bc
}

# Prepare input/output files.
IN_FILE=${1}
in_filename=$(basename $IN_FILE)
FILE_EXT="${in_filename##*.}"
TEMP_FILE="temp.${FILE_EXT}"
OUT_FILE=${4}

# Prepare starting position and duration.
SS=$( to_s ${2} )
TT=$( to_s ${3} )
OFFSET=3
xSS=`expr $SS - $OFFSET`
xTT=`expr $TT + $OFFSET + $OFFSET`

echo $'\n================================================================================'
echo "  IN_FILE:${IN_FILE} -ss:${xSS} -t:${xTT} TEMP_FILE:${TEMP_FILE}"
echo $'================================================================================\n'
ffmpeg -i $IN_FILE -ss $xSS -t $xTT -async 1 -c copy -avoid_negative_ts 1 -strict -2 $TEMP_FILE

echo $'\n================================================================================'
echo "  TEMP_FILE:${TEMP_FILE} -ss:${OFFSET} -t:${TT} OUT_FILE:${OUT_FILE}"
echo $'================================================================================\n'
ffmpeg -i $TEMP_FILE -ss $OFFSET -t $TT -async 1 -avoid_negative_ts 1 -strict -2 $OUT_FILE
